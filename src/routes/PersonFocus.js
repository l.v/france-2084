import React, { useState, useEffect } from 'react'
import { Item } from 'semantic-ui-react'
import { useRouteMatch, useParams } from "react-router-dom";
import { getUser } from "../api";
import PeopleList from '../routes/PeopleList';

export default function PersonFocus(props) {
  const match = useRouteMatch();
  const {personGUID} = useParams();
  const [person, setPerson] = useState(null);

  useEffect(() => {
    getUser(personGUID)
    .then(res => {
      setPerson(res.data);
    })
    //TODO catch
  }, []);

  console.log("RouteMatch:", match)
  console.log("GUID:", personGUID);
  console.log("Person:", person);

  if (!person) {
    return <h1>no user found with this guid (TODO: proper error page)</h1>
  }

    //fix id => _id
    if (person.friends)
      person.friends.forEach(friend => friend._id = friend.id)

    return <Item>
      <Item.Image size='tiny' src={person.picture} />

      <Item.Content>
        <Item.Header>{person.name.first} {person.name.last}</Item.Header>
        <Item.Meta>
          <span className='eye-color'>{person.eyeColor}</span>
          <span className='age'>{person.age}</span>
        </Item.Meta>
        <Item.Description>{person.about}</Item.Description>
        <textarea style={{width:"100%", height: "600px"}} value={JSON.stringify(person, null, 4)} readOnly></textarea>

        { person.friends &&
            <PeopleList people={person.friends}/>
        }

      </Item.Content>
    </Item>
}
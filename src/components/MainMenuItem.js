import React from 'react';
import { Grid, Icon } from 'semantic-ui-react';
import { Link } from "react-router-dom";

export default function ({to, name, label, loading, disabled}) {
    return <Link to={to}>
        <Grid textAlign="center">
            <Grid.Row>
                <Icon size="massive" name={name} loading={loading} disabled={disabled}/>
            </Grid.Row>
            <Grid.Row>
                {label}
            </Grid.Row>
        </Grid>
    </Link>
}
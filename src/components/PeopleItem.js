import React from 'react'
import { List, Image } from 'semantic-ui-react'
import { Link } from "react-router-dom";

export default function PeopleItem({person}) {
    return <List.Item>
        <Image avatar size="tiny" src={person.picture} />
        <List.Content>
            <List.Header as={Link} to={'/person/' + person.guid}>
                {person.name.first} {person.name.last}
            </List.Header>
            <List.Description>
                {person.guid}
            </List.Description>
        </List.Content>
    </List.Item>
}
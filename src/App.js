import React, { useState, useEffect } from 'react';
import MainMenu from "./routes/MainMenu";
import PeopleList from './routes/PeopleList';
import PersonFocus from './routes/PersonFocus';
import 'semantic-ui-css/semantic.min.css';
import { Container, Search } from 'semantic-ui-react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import { getPeople } from "./api";

function App(props) {
    const [people, setPeople] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
      getPeople(50)
      .then((res) => {
        setLoading(false);
        setPeople(res.data);
      })
      .catch((error) => {
        //TODO
      })
    }, [])

    return <Router>
      <Container>
        <Switch>
          <Route path="/people">
            <Search placeholder="TODO" />
            <PeopleList people={people}/>
          </Route>

          {
            people.map((person) =>
              <Route key={person._id} path='/person/:personGUID'>
                  <PersonFocus person={person} />
              </Route>
            )
          }

          <Route path="/">
            <MainMenu />
          </Route>
        </Switch>
      </Container>
    </Router>;
}

export default App;
